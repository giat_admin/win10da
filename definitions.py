'''
Created on Jul 26, 2018

@author: bkotamahanti
'''

import os

ROOT_DIR = os.path.dirname(os.path.realpath(__file__))
LOG_DIR = "\\com\\gilead\\logs\\"
NOTEPAD_DIR = "\\com\\gilead\\notepad\\"
SEP_DIR = "\\com\\gilead\\sep\\"
CALCULATOR_DIR = "\\com\\gilead\\calculator\\"
WEBDRIVER_DIR = "\\com\\gilead\\utils\\winium_driver\\"
PAINT_DIR="\\com\\gilead\\paint\\"

'''SEP properties '''
SEP_PROCESS_NAME = "SymCorpUI.exe"
SEP_RUN_ACTIVE_SCAN_SUB_PROCESS = "SavUI.exe"

''' Calculator properties '''
CALCULATOR_PROCESS_NAME = "Calculator.exe"

cal_dict = {    0   : {
                        'Name'  :  "Zero",
                        'AutomationId' : "num0Button",
                        'IsEnabled' :  True,
                        'ClassName' :  "Button" },
                1   : {
                        'Name'  :  "One",
                        'AutomationId' : "num1Button",
                        'IsEnabled' :   True,
                        'ClassName' :  "Button" },
                2   : {   
                        'Name'  :  "Two",
                        'AutomationId' : "num2Button",
                        'IsEnabled' :   True,
                        'ClassName' :   "Button" },
                3   : {
                        'Name'  :  "Three",
                        'AutomationId' : "num3Button",
                        'IsEnabled' :   True,
                        'ClassName' :   "Button" },
                4   : {
                        'Name'  :  "Four",
                        'AutomationId' : "num4Button",
                        'IsEnabled' :   True,
                        'ClassName' :   "Button"},
                5   : {
                        'Name'  :  "Five",
                        'AutomationId' : "num5Button",
                        'IsEnabled' :   True,
                        'ClassName' :   "Button"},
                6   : {
                        'Name'  :  "Six",
                        'AutomationId' : "num6Button",
                        'IsEnabled' :   True,
                        'ClassName' :   "Button"},
                7   : {
                        'Name'  :  "Seven",
                        'AutomationId' : "num7Button",
                        'IsEnabled' :   True,
                        'ClassName' :   "Button"},
                8   : {
                        'Name'  :  "Eight",
                        'AutomationId' : "num8Button",
                        'IsEnabled' :   True,
                        'ClassName' :   "Button"},
                9   : {
                        'Name'  :  "Nine",
                        'AutomationId' : "num9Button",
                        'IsEnabled' :   True,
                        'ClassName' :   "Button"}          
            }    
                        
'''Outlook properties '''
email_recepient_id = "bindu.kotamahanti@gilead.com"
email_subject = "This is test Subject"
email_content = "Hello World!! This is Sample message to the recipient"


            
            
            