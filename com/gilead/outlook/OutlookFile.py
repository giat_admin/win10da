'''
Created on Aug 24, 2018

@author: bkotamahanti
'''
from selenium import webdriver

class OutlookTestClass():
    __process = "OUTLOOK.EXE"
    def __init__(self, utilsRef, loggerRef):
        self.utils = utilsRef
        self.logger = loggerRef
        self.driver = ""
    
    
    def connectOutlookToWebDriver(self):
        self.driver = webdriver.Remote(command_executor="http://localhost:9999",
                         desired_capabilities={
                             "app" :  r"C:\\Program Files (x86)\\Microsoft Office\\Office16\\OUTLOOK.exe",
                                               "debugConnectToRunningApp": 'true',
                                               "args": "-port 345"
                             
                             })
        
    
    def killProcess(self):
        self.utils.killProcess(self.__process)
    
    def startOutlook_via_StartMenu(self):
        self.utils.startProgramByName_via_StartMenu("Outlook")
    
    def isOutlookProcessExist(self):
        return self.utils.isProcessExist(self.__process)
    
    def getOutlookMainWindowDoctopPaneElement(self):
        parent_element = self.driver.find_element_by_class_name("rctrl_renwnd32")
        MsoDockTop_element = parent_element.find_element_by_name("MsoDockTop")
        return MsoDockTop_element
    
    
    def clickOnEmailDraftFolder(self):
        Mail_folders_element = self.driver.find_element_by_name("Mail Folders")
        Mail_folders_element.find_element_by_name("Drafts").click()
        
        
    def getOutlookUntitledWindowDoctopPaneElement(self):
        compose_mail_window_element = self.driver.find_element_by_class_name("rctrl_renwnd32")
        dockTop_element = compose_mail_window_element.find_element_by_name("MsoDockTop")
        return dockTop_element
    
    def isOutlookElementExist(self, webelement , **property1):
        return self.utils.isElementPresent( webelement,  **property1)
    
    def getTodaysEmailGroupElement(self):
        table_view_element = self.getOutlookTableViewElement()
        table_element_today = table_view_element.find_element_by_name("Group By: Expanded: Date: Today")
        return table_element_today
    

    def isOutlookSentEmailElementExist(self):
        table_element_today = self.getTodaysEmailGroupElement()
        elements = table_element_today.find_elements_by_class_name("LeafRow")
        for element in elements:
            print(element.get_attribute("Name"))
            if "This is test Subject" in element.get_attribute("Name"):
                return True
            else:
                continue
        return False

    def getOutlookTableViewElement(self):
        parent_element = self.driver.find_element_by_class_name("rctrl_renwnd32")
        table_view_element = parent_element.find_element_by_name("Table View")
        return table_view_element
    
    
    def isOutlookDraftedEmailElementExist(self):
        table_view_element = self.getOutlookTableViewElement()
        elements = table_view_element.find_elements_by_class_name("LeafRow")
        for element in elements:
            print(element.get_attribute("Name"))
            if "This is test Subject" in element.get_attribute("Name"):
                return True
            else:
                continue
        return False
    
    
    def clickEmailDraftedInOutlookMainWindow(self):
        table_view_element = self.getOutlookTableViewElement()
        elements = table_view_element.find_elements_by_class_name("LeafRow")
        for element in elements:
            print(element.get_attribute("Name"))
            if "This is test Subject" in element.get_attribute("Name"):
                element.click()
                break
            else:
                continue
        return False
    
    
    def clickNewMail(self):
        MsoDockTop_element = self.getOutlookMainWindowDoctopPaneElement()
        MsoDockTop_element.find_element_by_name("New Email").click()
    
    def clickEmailReceivedInOutlookMainWindow(self):
        table_element_today = self.getTodaysEmailGroupElement()
        elements = table_element_today.find_elements_by_class_name("LeafRow")
        for element in elements:
            print(element.get_attribute("Name"))
            if "This is test Subject" in element.get_attribute("Name"):
                element.click()
                element.submit()
                break
            else:
                continue
    def deleteEmailReceived(self):
        ribbon_pane_element = self.driver.find_element_by_class_name("rctrl_renwnd32").find_element_by_name("MsoDockTop").find_element_by_class_name("NetUInetpane")
        lower_ribbon_pane_element = ribbon_pane_element.find_element_by_name("Lower Ribbon")
        delete_group_element = lower_ribbon_pane_element.find_element_by_name("Message").find_element_by_name("Delete")
        elements = delete_group_element.find_elements_by_class_name("NetUIRibbonButton")
        for element in elements:
            if element.get_attribute("Name") == "Delete" :
                element.click()
                break

    def clickFileTabInOutlookMainWindow(self):
        self.driver.find_element_by_name("File Tab").click()

    def clickExitMenuOption(self):
        self.clickFileTabInOutlookMainWindow()
        file_menu_element = self.driver.find_element_by_name("Backstage view").find_element_by_name("File")
        file_menu_element.find_element_by_name("Exit").click()


    def clickFileTabInUntitledWindow(self):
        MsoDockTop_element = self.getOutlookUntitledWindowDoctopPaneElement()
        MsoDockTop_element.find_element_by_name("File Tab").click()
        self.logger.info("Clicked on New Email - > File Tab")


    def getFileMenuOptionsOfUntitledWindow(self):
        compose_mail_window_element = self.driver.find_element_by_class_name("rctrl_renwnd32")
        file_menu_element = compose_mail_window_element.find_element_by_name("File")
        return file_menu_element

    def clickCloseMenuOptionOfUntitledWindow(self):
        self.clickFileTabInUntitledWindow()
        file_menu_element = self.getFileMenuOptionsOfUntitledWindow()
        file_menu_element.find_element_by_name("Close").click()
        self.logger.info("Clicked on Untitled message window - > File Tab- > Close")
    

    def getEmailFormRegionElement(self):
        compose_mail_window_element = self.driver.find_element_by_class_name("rctrl_renwnd32")
        form_region = compose_mail_window_element.find_element_by_class_name("AfxWndW")
        return form_region

    def enterEmail__id(self, email_id):
        form_region = self.getEmailFormRegionElement() 
        to_element = form_region.find_element_by_name("To")
        to_element.send_keys(email_id)
    
    def enterEmail_subject(self, email_subject):
        form_region = self.getEmailFormRegionElement() 
        subject_element = form_region.find_element_by_xpath("//*[@AutomationId='4101' and @Name='Subject']")
        subject_element.send_keys(email_subject)
    
    def enterEmail_message_and_click_on_Send(self, email_message):
        form_region = self.getEmailFormRegionElement() 
        message_element = form_region.find_element_by_class_name("_WwG")
        message_element.send_keys(email_message)
        form_region.find_element_by_name("Send").click()
    
    def enterEmail_message(self, email_message):
        form_region = self.getEmailFormRegionElement() 
        message_element = form_region.find_element_by_class_name("_WwG")
        message_element.send_keys(email_message)
    
    
    def clickSaveIconOnUntitledMessageWindow(self):
        dockTop_element = self.getOutlookUntitledWindowDoctopPaneElement()
        OuickAccessToolBar_element = dockTop_element.find_element_by_name("Quick Access Toolbar")
        OuickAccessToolBar_element.find_element_by_name("Save").click()
    
    
    def clickEmailSend(self):
        form_region = self.getEmailFormRegionElement() 
        send_element = form_region.find_element_by_xpath("//*[@AutomationId='4256' and @Name='Send']")
        send_element.click()
