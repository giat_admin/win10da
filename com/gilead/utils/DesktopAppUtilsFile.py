'''
Created on Jul 23, 2018

@author: bkotamahanti
'''
import psutil
import pyautogui
import time
import logging
import inspect
import subprocess
from definitions import ROOT_DIR, WEBDRIVER_DIR
from _overlapped import NULL
import definitions
import os


class DesktopAppCommonFunctionsClass(object):
    wait_time = 2
    
    def __init__(self):
        self.logger = logging
        self.driver = NULL
    
    def killProcess(self, process_name):
        for proc in psutil.process_iter():
            # check whether the process_name name matches
            if proc.name() == process_name:
                self.logger.info("Found the process "+process_name+ " in process list and killing it now")
                proc.kill()
                self.logger.info("Killed the process "+process_name)
                
    def startWebDriver(self):
        return subprocess.Popen(ROOT_DIR+WEBDRIVER_DIR+"Winium.Desktop.Driver.exe")           
    
    
    def removeFile(self, filename):
        if os.path.exists(filename):
            os.remove(filename)
            self.logger.info("File {} is deleted successfully ".format(filename))
        
    
    def isElementPresent(self, driver_element, **kargs):
#         self.driver = driver
        if 'Name' in kargs:
            list_of_webelements= driver_element.find_elements_by_name(kargs['Name'])
        if 'ClassName' in kargs:
            list_of_webelements= driver_element.find_elements_by_class_name(kargs['ClassName'])
        
        print("---->number of elements {}".format(len(list_of_webelements)))
        return len(list_of_webelements) > 0
    
    def isProcessExist(self,process_name):
        for proc in psutil.process_iter():
            # check whether the process_name name matches
            if proc.name() == process_name:
                self.logger.info("process "+str(process_name)+" exists")
                return True
        self.logger.info("process "+str(process_name)+" doesn't exist anymore")
        return False
    
    def startProgramByName_via_StartMenu(self, progName):
        pyautogui.press("winleft")
        self.sleepUntil(self.wait_time)
        pyautogui.typewrite(progName)

        pyautogui.press("enter")
        self.sleepUntil(self.wait_time)
        self.logger.info("Started program "+str(progName)+" via Windows start icon")
        return True
    
    def getCoordinatesByLocatingGivenImageOnScreen(self,imageFile, minSearchTime):
        r=pyautogui.locateOnScreen(imageFile, minSearchTime)
        self.logger.info("image is located at coordinates on screen ==> "+str(r)+" <==")
        time.sleep(self.wait_time)
        a= r[0] 
        b= r[1]
        self.logger.info("returning coordinates of image==> "+str(imageFile)+" <==")
        return a,b
    
    def moveToLocationAndClick(self, x_coor, y_coor):
        self.logger.info("moving to location ( {} {} ) on application screen:".format(x_coor, y_coor))
        pyautogui.moveTo(x_coor , y_coor)
        self.logger.info("clicking on location ( {} {} ) of application screen:".format(x_coor, y_coor))
        pyautogui.click(x_coor, y_coor)
    
    def isAppStarted(self, imageFile, searchTime):
        r = pyautogui.locateOnScreen(imageFile, 20)
        if(r is not None):
            self.logger.info("able to locate image {} on the application".format(imageFile))
            return True
        self.logger = self.logger.getLogger()
        self.logger.debug("not able to locate image {} on the application".format(imageFile))
        return False
    
    def sleepUntil(self, wait):
        time.sleep(wait)
    
    
    def log_assert(self, bool_, message="", logger=None, logger_name="", verbose=False ):
        """Use this as a replacement for assert if you want the failing of the
    assert statement to be logged."""
        if logger is None:
            logger = logging.getLogger(logger_name)
        try:
            assert bool_, message
        except AssertionError as e:
            last_stackframe = inspect.stack()[-2]
            source_file, line_no, func = last_stackframe[1:4]
            source = "Traceback (most recent call last):\n" + \
                    '  File "%s", line %s, in %s\n    ' % (source_file, line_no, func)
            if verbose:
                # include more lines than that where the statement was made
                source_code = open(source_file).readlines()
                source += "".join(source_code[line_no - 3:line_no + 1])
            else:
                source += last_stackframe[-2][0].strip()
            logger.debug("%s\n%s" % (message+":"+str(e), source))
            raise AssertionError("%s\n%s" % (message, source))