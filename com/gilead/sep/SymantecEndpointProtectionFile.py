'''
Created on Jul 25, 2018

@author: bkotamahanti
'''
import pyautogui
import time
import definitions
from definitions import SEP_PROCESS_NAME
from definitions import SEP_RUN_ACTIVE_SCAN_SUB_PROCESS

class SEPTestClass(object):
    minSearchTime = 5
    wait_time = 2
    def __init__(self, utilsRef, loggerRef):
        self.images = definitions.ROOT_DIR + definitions.SEP_DIR + "images\\"
        self.utils = utilsRef
        self.logger = loggerRef
    
    def startSEP_via_StartMenu(self):
        return self.utils.startProgramByName_via_StartMenu("Symantec")
    
    def is_SEP_run_active_scan_sub_process_started(self):
        return self.utils.isProcessExist( SEP_RUN_ACTIVE_SCAN_SUB_PROCESS )    
    
    def is_SEP_processExist(self):    
        return self.utils.isProcessExist(SEP_PROCESS_NAME)
        
    def is_SEP_app_started(self, imageFile):
        return self.utils.isAppStarted(self.images+imageFile, self.minSearchTime)   
        
    def getCoordinatesByLocatingGivenImageOnScreen(self,imageFile):
        return self.utils.getCoordinatesByLocatingGivenImageOnScreen(self.images+imageFile, self.minSearchTime)
    
    def moveToLocationAndClick(self, x_coor, y_coor):
        self.utils.moveToLocationAndClick(x_coor, y_coor)

    def closeSEP(self):
        pyautogui.moveRel(90,-15)
        time.sleep(self.wait_time)
        self.logger.info("Closing SEP application" )
        pyautogui.click()