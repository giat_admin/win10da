'''
Created on Aug 17, 2018

@author: bkotamahanti
'''

''' built-in modules'''
import os
import time
import logging
import inspect
from selenium.common.exceptions import WebDriverException,\
    NoSuchElementException
from pywinauto import MatchError
from _overlapped import NULL

'''user-defined modules'''
from com.gilead.notepad.NotepadFile import NotepadTestClass
from com.gilead.utils.DesktopAppUtilsFile import DesktopAppCommonFunctionsClass
from com.gilead.sep.SymantecEndpointProtectionFile import SEPTestClass
from com.gilead.calculator.CalculatorFile import CalculatorTestClass
from com.gilead.paint.PaintFile import PaintTestClass
from com.gilead.outlook.OutlookFile import OutlookTestClass
from definitions import ROOT_DIR, LOG_DIR, NOTEPAD_DIR, email_recepient_id, email_subject, email_content

class Main():
    fh = NULL
    def __init__(self):
        '''public variables'''
        
        self.logFile = ROOT_DIR + LOG_DIR + "testLogs.txt"
        ''' Checking and removing the log file before opening and writing to it...'''
        if(self.fh == NULL):   
            if (os.path.exists(self.logFile)):
                os.remove(self.logFile) 
#             self.fh = open(self.logFile, "w+")
#             self.fh.close()
          
        self.logger = logging.getLogger("root")
        for handler in logging.root.handlers[:]:
            logging.root.removeHandler(handler)
        
        logging.basicConfig(filename = self.logFile, 
                                level=logging.DEBUG, 
                                format='[%(asctime)s : %(levelname)s : filename ( %(filename)s ): line number ( %(lineno)s )-------function name ( %(funcName)s()) ] %(message)s' 
                                )
        ''' private variables'''
        self.__num1 = 0
        self.__num2 = 0
      
        ''' object initialization '''
        self.utils = DesktopAppCommonFunctionsClass()
        self.notepad = NotepadTestClass(self.utils, self.logger)
        self.sep = SEPTestClass(self.utils, self.logger)
        self.calculator = CalculatorTestClass(self.utils, self.logger)
        self.paint = PaintTestClass(self.utils, self.logger)
        self.outlook = OutlookTestClass(self.utils, self.logger)
        
        ''' Kill the webdriver process if any before being used by test cases'''
        self.utils.killProcess("Winium.Desktop.Driver.exe")    
        
    '''TC: W10DA-1 '''
    def notepad_save_and_open_file_testcase_id_W10DA_1(self):
        try:
            func = inspect.currentframe().f_back.f_code
            self.logger.info("Starting execution of function {} :".format(func.co_name))
            self.logger.info("Killing Notepad process before executing test case")
            #TC Step 1
            self.notepad.killProcess()
            
            #TC Step 2
            self.logger.info("Starting Notepad.exe process")
            self.notepad.startNotepadProcess()
            assert self.notepad.isNotepadProcessExist(),"Notepad.exe is not started"
            self.logger.info("Notepad process started successfully")
            #TC Step 3
            self.logger.info("Entering some text in Untitled-Notepad")
            self.utils.sleepUntil(2)
            self.notepad.enterTextFromAnotherFile("inputFile.txt")
            self.logger.info("Entering text is done...")
            #TC Step 4
            self.logger.info("Selecting the Menu item File->Exit..")
            self.utils.sleepUntil(2)
            self.notepad.selectMenu("File->Exit")
            self.logger.info("Selected File->Exit Menu")
            self.utils.sleepUntil(2)
            x_coordinate, y_coordinate = self.notepad.getCoordinatesByLocatingGivenImageOnScreen("notepad_pop_up_image.PNG")
            assert (x_coordinate != 0 and y_coordinate !=0) ,"either of the coordinates are empty" 
            self.logger.info("Able to locate the pop-up image after File->Exit")
            self.logger.info("clicking Save ")
            #TC Step 5
            self.notepad.clickSave()
            self.utils.sleepUntil(2)
            #TC Step 6
            self.notepad.enterFileNameAndClickSave("SavedTextFile.txt")
            #TC Step 7
            self.utils.sleepUntil(2)
            self.notepad.selectMenu("File->Exit")
            self.logger.info("Closed notepad process after saving")
            assert os.path.exists(ROOT_DIR+NOTEPAD_DIR+"SavedTextFile.txt"), "File SavedTextFile.txt doesn't exist in the given file path:\""+ROOT_DIR+NOTEPAD_DIR+"\""
            
            self.logger.info("Starting notepad process again")
            #TC Step 8
            self.notepad.startNotepadProcess(),"Notepad is not started"
            #TC Step 9
            assert self.notepad.isNotepadProcessExist(),"Notepad.exe is not started"
            #TC Step 10, 11
            self.utils.sleepUntil(2)
            self.notepad.openFile("SavedTextFile.txt")#remove created files-clean up
            #TC Step 12 
            self.utils.sleepUntil(2)
            assert self.notepad.compareInputAndOutputFiles("inputFile.txt","SavedTextFile.txt"), "The input file inputFile.txt didnt match with output file SavedTextFile.txt"
            self.logger.info("The input file inputFile.txt matched with output file SavedTextFile.txt")
            #TC Step 13
            self.notepad.selectMenu("File->Exit")
            self.logger.info("Closed the application....")
            return True, "", func.co_name
        except (AssertionError, AttributeError, TypeError, MatchError) as e:
            return False, e, func.co_name
    
    
    '''TC: W10DA-2 '''    
    def notepad_dont_save_file_testcase_id_W10DA_2(self):
        try:
            func = inspect.currentframe().f_back.f_code
            self.logger.info("Starting execution of function {} :".format(func.co_name))
            self.logger.info("Killing Notepad process before executing test case")
            #TC Step 1
            self.notepad.killProcess() #check their is no process by name notepad.exe using os.
        
            #TC Step 2
            self.notepad.startNotepadProcess()
            assert self.notepad.isNotepadProcessExist(),"Notepad.exe is not started"
            self.logger.info("Notepad process started successfully")
            #TC Step 3
            self.utils.sleepUntil(2)
            self.logger.info("Entering some text in Untitled-Notepad")
            self.notepad.enterTextFromAnotherFile("inputFile.txt")
            self.logger.info("Entering text is done...")
            #TC Step 4
            self.logger.info("Selecting the Menu item File->Exit..")
            self.utils.sleepUntil(2)
            self.notepad.selectMenu("File->Exit")
            self.utils.sleepUntil(2)
            #TC Step 5 
            self.notepad.clickDontSave()
            self.logger.info("Clicked Dont Save")
            #TC Step 6
            assert (self.notepad.isNotepadProcessExist() == False), "Notepad.exe file didn't close even after not saving file" 
            return True, "", func.co_name
        
        except (AssertionError, AttributeError, TypeError, MatchError) as e:
            return False, e, func.co_name
        
        
    '''TC:  W10DA-3 '''    
    def notepad_save_delete_open_file_testcase_id_W10DA_3(self): 
        try:
            func = inspect.currentframe().f_back.f_code
            self.logger.info("Starting execution of function {} :".format(func.co_name))
            
            '''start webdriver '''
            pid = self.utils.startWebDriver()
            self.utils.sleepUntil(3)
            self.logger.info("Launched the WebDriver of notepad app ")
            
            self.logger.info("Killing Notepad process before executing test case")
            self.utils.killProcess("notepad.exe") #check their is no process by name notepad.exe using os.
            
            #TC Step 1
            self.notepad.startNotepad_via_StartMenu()
            self.utils.sleepUntil(2)
            assert self.notepad.isNotepadProcessExist(), "Notepad process notepad.exe didn't exist "
            
            ''' Connect the already running notepad process to winium Driver instead of opening new one.'''
            self.notepad.connectNotepadToWebDriver()
            self.logger.info("connected webdriver to notepad process...")
            
            #TC Step 2
            self.utils.sleepUntil(5)
            self.logger.info("entering text in notepad")
            self.notepad.enterTextFromAnotherFileUsingWebdriver("inputFile.txt")
            
            
            #TC Step 3
            self.notepad.clickMenuOptionSave()
            self.utils.sleepUntil(2)
            assert self.notepad.getAttribute("#32770") == "Save As", "Save As dialog didn't open"
            
            #TC Step 4
            self.notepad.saveFileAs("sample.txt")
            
            #TC Step 5
            self.notepad.closeNotepadApp()
            self.utils.sleepUntil(2)
            
            #TC Step 6
            self.notepad.removeTextFile("sample.txt")
            
            #TC Step 7
            self.notepad.startNotepad_via_StartMenu()
            self.utils.sleepUntil(2)
            assert self.notepad.isNotepadProcessExist(), "Notepad process notepad.exe didn't exist "
            self.notepad.clickOpenMenuOptionViaWebdriver()
            self.utils.sleepUntil(2)
            assert self.notepad.getAttribute("#32770") == "Open", "Open dialog didn't open"
            
            #TC Step 8
            self.notepad.enterFileNameAndClickOpenViaWebdriver("sample.txt")
            
            #TC Step 9
            self.utils.sleepUntil(2)
            
            x_coordinate, y_coordinate = self.notepad.getCoordinatesByLocatingGivenImageOnScreen("notepad_pop_up_image_file_not_found.PNG")
            assert (x_coordinate != 0 and y_coordinate !=0) ,"either of the coordinates are empty" 
            self.logger.info("clicking OK ")

            self.notepad.clickOK()
            self.logger.info("Clicked on Ok button")
            
            self.notepad.clickCancel()
            self.logger.info("clicked Cancel")
            self.notepad.closeNotepadApp()
            self.logger.info("Closed Notepad app..")
            
            return True, "", func.co_name
        except (AssertionError, AttributeError, TypeError, MatchError) as e:
            return False, e, func.co_name
               
        finally:
            '''kill the web driver '''
            pid.terminate()
            self.logger.info("Killed the webDriver process of notepad app")
    
    
    '''TC:  W10DA-4 '''
    def calculator_open_perform_arithemetic_operations_close_app_testcase_id_W10DA_4(self):
        try:
            func = inspect.currentframe().f_back.f_code
            self.logger.info("Starting execution of function {} :".format(func.co_name))
            
            '''start webdriver '''
            pid = self.utils.startWebDriver()
            self.logger.info("Launched the WebDriver of calculator app ")
            
            self.logger.info("Starting execution of function :"+str(func.co_name))
            #TC Step 1
            self.logger.info("Checking and killing the process Calculator.exe before executing the test case")
            self.calculator.killProcess() 
            #TC Step 2 
            self.calculator.startCalculator_via_StartMenu()
            self.utils.sleepUntil(4)
            assert self.calculator.isCalculatorProcessExist(), "Calculator process Calculator.exe didn't exist "
            
            ''' Connect the already running calculator process to winium Driver instead of opening new one.'''
            self.calculator.connectCalculatorToWebDriver()
            
            self.__num1 = 3
            self.__num2 = 4
            #TC Step 3
            self.calculator.addGivenNumbers(3,4)
            '''waiting to start next operation'''
            self.utils.sleepUntil(1)
            assert '7' in self.calculator.getCalculatorResults(), "CalculatorResults:"+self.calculator.getCalculatorResults()+" didn't match expected result:"+str(self.__num1)+"+"+str(self.__num2)+"="+(str(self.__num1+self.__num2))
            self.logger.info("Addition performed successfully")
            #TC Step 4
            self.calculator.substractGivenNumbers(7,3)
            '''waiting to start next operation'''
            self.utils.sleepUntil(1)
            assert '4' in self.calculator.getCalculatorResults(), "CalculatorResults:"+self.calculator.getCalculatorResults()+" didn't match expected result:"+str(self.__num1)+"-"+str(self.__num2)+"="+(str(self.__num1-self.__num2))
            self.logger.info("Subtraction performed successfully")
            
            #TC Step 5
            self.calculator.multiplyGivenNumbers(5,3)
            '''waiting to start next operation'''
            self.utils.sleepUntil(1)
            assert '15' in self.calculator.getCalculatorResults(), "CalculatorResults:"+self.calculator.getCalculatorResults()+" didn't match expected result:"+str(self.__num1)+"*"+str(self.__num2)+"="+(str(self.__num1*self.__num2))
            self.logger.info("Multiplication performed successfully")
            
            #TC Step 6
            self.calculator.divideGivenNumbers(6,2)
            assert '3' in self.calculator.getCalculatorResults(), "CalculatorResults:"+self.calculator.getCalculatorResults()+" didn't match expected result:"+str(self.__num1)+"/"+str(self.__num2)+"="+(str(self.__num1/self.__num2))
            self.logger.info("Division performed successfully")
            self.calculator.killProcess()
            assert (self.calculator.isCalculatorProcessExist()==False), "Calculator process Calculator.exe didn't exist anymore "
            self.logger.info("Calculator process Calculator.exe didn't exist anymore ...")
            return True, "", func.co_name
        
        except (AssertionError, AttributeError, TypeError) as e:
            return False, e, func.co_name
        
        finally:
            '''kill the web driver '''
            pid.terminate()
            self.logger.info("Killed the webDriver process of calculator app")
    
    
    
    '''TC: W10DA-5 '''
    def paint_open_do_paint_close_save_app_open_testcase_id_W10DA_5(self):
        try:
            func = inspect.currentframe().f_back.f_code
            self.logger.info("Starting execution of function {} :".format(func.co_name))
            
            '''start webdriver '''
            pid = self.utils.startWebDriver()
            self.logger.info("Launched the WebDriver of Paint app... ")
            
            #TC Step 1
            self.logger.info("Checking and killing the process mspaint.exe before executing the test case")
            self.paint.killProcess()
            #TC Step 2 
            self.paint.startPaint_via_StartMenu()
            self.utils.sleepUntil(2)
            assert self.paint.isPaintProcessExist(), "Paint process mspaint.exe didn't exist"
            
            ''' Connect the already running paint process to winium Driver instead of opening new one.'''
            self.paint.connectPaintToWebDriver()
            assert (self.paint.getPaintAppName()=="Untitled - Paint"), "Untitled Paint didn't open"
            self.logger.info("Paint app opened..")
            
            #TC Step 3 
#             self.paint.maximizePaintApp()
#             self.logger.info("maximized Paint window...")
            
            #TC Step 4
            self.paint.drawSomethingOnEmptyPaintBoard()
            self.logger.info("Take screenshot before saving....")
            
            ''' Take screenshot after drawing something. we will use to compare it later'''
            coordinates = (450, 350, 350, 300)
            self.paint.takeScreenshot(*coordinates, "shot1.jpg")
            
            #TC Step 5
            self.paint.clickExitMenuOptionInPaintApp()
            assert (self.paint.getTextFromPopUpWindow() == "Do you want to save changes to Untitled?"), "pop-up didn't display"
            self.logger.info("pop-up displayed with Save, Don't Save, Cancel options")
            
            #TC Step 6
            self.paint.clickSaveOnPopUp()
            self.logger.info("clicked Save button on pop-up window")
            
            ''' validating Save As dialog properties '''
            properties_list = [("#32770","Name"), ("AppControlHost", "Name")]
            self.logger.info("Identifying the properties {} of Save as dialog".format(properties_list))
            x,y=self.paint.getAttributesValuesOfSaveAsDialog(*properties_list)
            assert (x,y) == ("Save As","File name:"), "Values of attributes ({} , {}) didn't match in Save As Dialog with ({}, {}). Something went wrong..".format("Save As","File name:",x,y)
            self.logger.info("Save As dialog displayed...")
            
            #TC Step 7
            self.paint.enterFileNameAndClickSave("SavedPaintFile")
            time.sleep(2)
            assert ( self.paint.isPaintProcessExist()==False ), "Paint process mspaint.exe still exists even after closing the app"
            self.logger.info("mspaint.exe process is not running any more after closing Paint app.")

            #TC Step 8
            self.paint.startPaint_via_StartMenu()
            assert self.paint.isPaintProcessExist(), "Paint process mspaint.exe didn't exist"
            assert (self.paint.getPaintAppName()=="Untitled - Paint"), "Untitled Paint didn't open"
            self.logger.info("Paint app opened..")
            
            #TC Step 9
            self.paint.clickOpenMenuOptionViaWebdriver()
            self.utils.sleepUntil(1)
            self.paint.openPreviousSavedFile("SavedPaintFile")
            
            #TC Step 10
            assert (self.paint.getPaintAppName()=="SavedPaintFile - Paint"), "FileName didn't change after Saving File" 
            ''' take 2nd screenshot after opening file to compare with prev. image screenshot'''
            self.paint.takeScreenshot(*coordinates, "shot2.jpg") 
             
            '''compare the images '''
            assert self.paint.areImagesSame("shor1.jpg","shot2.jpg"), "Images are not same..."
            self.logger.info("Closing the Paint application...")
            self.paint.clickExitMenuOptionInPaintApp()
            self.logger.info("Clicked File close button...")
            return True, "", func.co_name
            
        except (AssertionError, AttributeError, TypeError, WebDriverException, NoSuchElementException) as e:
            return False, e, func.co_name
        
        finally:
            '''kill the web driver '''
            pid.terminate()
            self.logger.info("Killed the webDriver process of Paint app")
            
    
    '''TC: W10DA-6 '''        
    def paint_open_do_paint_close_dont_save_testcase_id_W10DA_6(self):
        try:
            func = inspect.currentframe().f_back.f_code
            self.logger.info("Starting execution of function {} :".format(func.co_name))
            
            '''start webdriver '''
            pid = self.utils.startWebDriver()
            self.logger.info("Launched the WebDriver of Paint app... ")
            
            #TC Step 1
            self.logger.info("Checking and killing the process mspaint.exe before executing the test case")
            self.paint.killProcess()
            #TC Step 2 
            self.paint.startPaint_via_StartMenu()
            self.utils.sleepUntil(2)
            assert self.paint.isPaintProcessExist(), "Paint process mspaint.exe didn't exist"
            
            ''' Connect the already running paint process to winium Driver instead of opening new one.'''
            self.paint.connectPaintToWebDriver()
            assert (self.paint.getPaintAppName()=="Untitled - Paint"), "Untitled Paint didn't open"
            self.logger.info("Paint app opened..")
            
            #TC Step 3
            self.paint.drawSomethingOnEmptyPaintBoard()
            
            #TC Step 4
            self.paint.clickExitMenuOptionInPaintApp()
            assert (self.paint.getTextFromPopUpWindow() == "Do you want to save changes to Untitled?"), "pop-up didn't display"
            self.logger.info("pop-up displayed with Save, Don't Save, Cancel options")
            
            #TC Step 5
            self.paint.clickDontSaveOnPopUp()
            self.logger.info("clicked Don't Save button on pop-up window")
            self.utils.sleepUntil(2)
            assert ( self.paint.isPaintProcessExist() == False ), "Paint process still running even after clicking on Dont Save button in Pop up"
            return True, "", func.co_name
        
        except (AssertionError, AttributeError, TypeError, WebDriverException, NoSuchElementException) as e:
            return False, e, func.co_name
            
        finally:
            '''kill the web driver '''
            pid.terminate()
            self.logger.info("Killed the webDriver process of Paint app")
            
            
    '''TC: W10DA-7 '''        
    def paint_save_delete_open_file_testcase_id_W10DA_7(self):
        try:
            func = inspect.currentframe().f_back.f_code
            self.logger.info("Starting execution of function {} :".format(func.co_name))
            
            '''start webdriver '''
            pid = self.utils.startWebDriver()
            self.logger.info("Launched the WebDriver of Paint app... ")
            
            #TC Step 1
            self.logger.info("Checking and killing the process mspaint.exe before executing the test case")
            self.paint.killProcess()
           
            self.paint.startPaint_via_StartMenu()
            self.utils.sleepUntil(2)
            assert self.paint.isPaintProcessExist(), "Paint process mspaint.exe didn't exist"
            
            ''' Connect the already running paint process to winium Driver instead of opening new one.'''
            self.paint.connectPaintToWebDriver()
            assert (self.paint.getPaintAppName()=="Untitled - Paint"), "Untitled Paint didn't open"
            self.logger.info("Paint app opened..")
            
            #TC Step 2
            self.paint.drawSomethingOnEmptyPaintBoard()
            
            
            #TC Step 3
            self.paint.clickSaveMenuOptionInPaintApp()
            self.utils.sleepUntil(2)
            
            ''' validating Save As dialog properties '''
            properties_list = [("#32770","Name"), ("AppControlHost", "Name")]
            self.logger.info("Identifying the properties {} of Save as dialog".format(properties_list))
            x,y=self.paint.getAttributesValuesOfSaveAsDialog(*properties_list)
            assert (x,y) == ("Save As","File name:"), "Values of attributes ({} , {}) didn't match in Save As Dialog with ({}, {}). Something went wrong..".format("Save As","File name:",x,y)
            self.logger.info("Save As dialog displayed...")
            
            #TC Step 4
            self.paint.enterFileNameAndClickSave("SamplePaintFile")
            time.sleep(2)
            
            #TC Step 5
            self.logger.info("Closing the Paint application...")
            self.paint.clickExitMenuOptionInPaintApp()
            self.logger.info("Clicked File close button...")
            self.utils.sleepUntil(2)
            
            assert ( self.paint.isPaintProcessExist() == False ), "Paint process still running even after closing the app"
            
            #TC Step 6
            self.logger.info("deleting the SamplePaintFile.png file ")
            self.paint.removePaintFile("SamplePaintFile.png")
            
            #TC Step 7
            self.paint.startPaint_via_StartMenu()
            self.utils.sleepUntil(2)
            assert self.paint.isPaintProcessExist(), "Paint process mspaint.exe didn't exist"
            assert (self.paint.getPaintAppName()=="Untitled - Paint"), "Untitled Paint didn't open"
            self.logger.info("Paint app opened..")
            
            #TC Step 8
            self.paint.clickOpenMenuOptionViaWebdriver()
            self.utils.sleepUntil(2)
            self.paint.openPreviousSavedFile("SamplePaintFile")
            
            x_coordinate, y_coordinate = self.paint.getCoordinatesByLocatingGivenImageOnScreen("paint_file_not_found_image.PNG")
            assert (x_coordinate != 0 and y_coordinate !=0) ,"either of the coordinates are empty" 
            
            #TC Step 9
            self.logger.info("clicking OK ")

            self.paint.clickOK()
            self.logger.info("Clicked on Ok button")
            
            self.paint.clickCancel()
            self.logger.info("clicked Cancel")
            self.paint.clickExitMenuOptionInPaintApp()
            self.logger.info("Closed Paint app..")
            
            return True, "", func.co_name
        
        except (AssertionError, AttributeError, TypeError, WebDriverException, NoSuchElementException) as e:
            return False, e, func.co_name
            
        finally:
            '''kill the web driver '''
            pid.terminate()
            self.logger.info("Killed the webDriver process of Paint app")     
    
    
    def getLocationOfGivenImageMoveAndClickElement(self, imageFile, x_offset, y_offset):
        x_coordinate, y_coordinate = self.sep.getCoordinatesByLocatingGivenImageOnScreen(imageFile)
        assert x_coordinate != 0 and y_coordinate != 0, "either of the coordinates are empty"
        self.sep.moveToLocationAndClick(x_coordinate + x_offset, y_coordinate+y_offset)
    
    def symantec_endpoint_protection_open_close_testcase_id_W10DA_8(self):
        try:
            func = inspect.currentframe().f_back.f_code
            self.logger.info("Starting execution of function {} :".format(func.co_name))
            #TC Step 1
            self.utils.killProcess("SymCorpUI.exe") #Need to check how to force kill the security process-Not working
            
            #TC Step 2
            self.utils.log_assert(self.sep.startSEP_via_StartMenu(), 
               "SEP app not started properly as it failed to enter program name in windows search bar", self.logger, 
               verbose=True) 
            
            assert self.sep.is_SEP_processExist(), "SEP process didn't start properly"
            
            #TC Step 3
            assert self.sep.is_SEP_app_started("202_screen1.PNG"), "SEP application not started properly"  
            
            #TC Step 4
            x_coordinate, y_coordinate = self.sep.getCoordinatesByLocatingGivenImageOnScreen("202_screen1.PNG")
            assert (x_coordinate != 0 and y_coordinate !=0) ,"either of the coordinates are empty as it could not locate image 202_screen1.PNG "
            
            self.sep.moveToLocationAndClick(x_coordinate , y_coordinate)
            self.sep.closeSEP()
            self.utils.sleepUntil(2)
            
            assert self.sep.is_SEP_processExist()== False,"SymCorpUI.exe didn't close"
            
            return True, "", func.co_name
        except (AssertionError, AttributeError, TypeError) as e:
            return False, e, func.co_name
    
    '''TC: W10DA-26 '''
    def symantec_endpoint_protection_run_active_scan_testcase_id_W10DA_26(self):
        try:
            func = inspect.currentframe().f_back.f_code
            self.logger.info("Starting execution of function {} :".format(func.co_name))
            #TC Step 1
            self.utils.killProcess("SymCorpUI.exe") #Need to check how to force kill the security process-Not working
            
            #TC Step 2
            self.utils.log_assert(self.sep.startSEP_via_StartMenu(), 
               "SEP app not started properly as it failed to enter program name in windows search bar", self.logger, 
               verbose=True) 
            
            assert self.sep.is_SEP_processExist(), "SEP process didn't start properly"
            assert self.sep.is_SEP_app_started("202_screen1.PNG"), "SEP application not started properly" 
            
            #TC Step 3
            # Identify and click on the image 'Scan for Threats' 
            self.getLocationOfGivenImageMoveAndClickElement("SEP_Scan_For_Threats.PNG", 30, 0)
            
            # Validate images on 'Scan for Threats' screen              
            x_coordinate, y_coordinate = self.sep.getCoordinatesByLocatingGivenImageOnScreen("SEP_Run_full_scan_text.PNG")
            assert (x_coordinate != 0 and y_coordinate !=0) ,"either of the coordinates are empty"
            x_coordinate, y_coordinate = self.sep.getCoordinatesByLocatingGivenImageOnScreen("SEP_Scan_For_Threats_text.PNG")
            assert (x_coordinate != 0 and y_coordinate !=0) ,"either of the coordinates are empty"
            
            #click on Run Active Scan Element 
            self.getLocationOfGivenImageMoveAndClickElement("SEP_Run_active_scan.PNG", 60, 13)
            
            #validate 'SEP Run active scan sub process'started or not
            assert self.sep.is_SEP_run_active_scan_sub_process_started(), "SEP Run active scan sub process didn't start"
            
            #TC Step 4
            #click on Pause scan button in sub process window
            self.getLocationOfGivenImageMoveAndClickElement("SEP_Run_active_scan_Pause_scan_image.PNG", 60, 13)
            self.utils.sleepUntil(1)
            
            #TC Step 5
            #click on Resume Scan button in sub process window
            self.getLocationOfGivenImageMoveAndClickElement("SEP_Run_active_scan_Resume_scan_image.PNG", 60, 13)
            self.utils.sleepUntil(1)
            #validate after Resume blue color Pause button image shows up, Completed text and Close button shows up
            x_coordinate, y_coordinate = self.sep.getCoordinatesByLocatingGivenImageOnScreen("SEP_Run_active_scan_PauseBlueColor_scan_image.PNG")
            assert (x_coordinate != 0 and y_coordinate !=0) ,"either of the coordinates are empty or 0"
            self.utils.sleepUntil(10)
            x_coordinate, y_coordinate = self.sep.getCoordinatesByLocatingGivenImageOnScreen("SEP_Run_active_scan_completed_image.PNG")
            assert (x_coordinate != 0 and y_coordinate !=0) ,"either of the coordinates are empty or 0"

            #TC Step 6
#           Close the sub-process window and validate   
            self.getLocationOfGivenImageMoveAndClickElement("SEP_Run_active_scan_close_image.PNG", 30, 13)
            
            #validate the sub process is closed or not
            self.utils.sleepUntil(2)
            assert (self.sep.is_SEP_run_active_scan_sub_process_started()== False), "SEP Run active scan sub process didn't close"
            
            #TC Step 7
            #close the SEP process
            self.getLocationOfGivenImageMoveAndClickElement("202_screen1.PNG", 0, 0)
            self.sep.closeSEP()
            
            return True, "", func.co_name
        except (AssertionError, AttributeError, TypeError) as e:
            return False, e, func.co_name
    
            
    '''TC: W10DA-19 '''            
    def outlook_compose_mail_check_mail_inbox_close_testcase_id_W10DA_19(self): 
        try:
            func = inspect.currentframe().f_back.f_code
            self.logger.info("Starting execution of function {} :".format(func.co_name))
            
            '''start webdriver '''
            pid = self.utils.startWebDriver()
            self.logger.info("Launched the WebDriver of Outlook app... ")
            
            
            #TC Step 1
            self.logger.info("Checking and killing the process OUTLOOK.exe if any before executing the test case")
            self.outlook.killProcess()
            
            #TC Step 2
            self.outlook.startOutlook_via_StartMenu()
            self.utils.sleepUntil(3)
            assert self.outlook.isOutlookProcessExist(), "Outlook process OUTLOOK.EXE didn't exist"
            
            ''' Connect the already running Outlook process to winium Driver instead of opening new one.'''
            self.outlook.connectOutlookToWebDriver()
            assert self.outlook.isOutlookElementExist(self.outlook.getOutlookMainWindowDoctopPaneElement(),**{'Name':'New Email'}), "Outlook app didnt open properly"
            
            self.logger.info("New Email Element is preset")
            
            #TC Step 3
            self.outlook.clickNewMail()
            self.logger.info("Clicked on New Email element in outlook")
            self.utils.sleepUntil(2)
            
            assert self.outlook.isOutlookElementExist(self.outlook.getOutlookUntitledWindowDoctopPaneElement(), **{'Name':'File Tab'}), "Untitled message window didnt show up"
            
            #TC Step 4
            self.outlook.clickCloseMenuOptionOfUntitledWindow()
            self.utils.sleepUntil(2)
            assert ( self.outlook.isOutlookElementExist(self.outlook.driver, **{'Name':'Untitled - Message (HTML) '})== False ), "Untitled message window didnt close yet"
            self.logger.info("New Compose mail closed successfully")
            assert self.outlook.isOutlookElementExist(self.outlook.getOutlookMainWindowDoctopPaneElement(), **{'Name':'New Email'}), "Outlook app didnt open properly"
            self.logger.info("Outlook Main window is showing up now...")
            
            #TC Step 5
            self.logger.info("Click New Email again...This time we would try to enter To, Subject and some message")
            self.outlook.clickNewMail()
            self.logger.info("Clicked New Email element again")
            self.utils.sleepUntil(2)
            assert self.outlook.isOutlookElementExist(self.outlook.getOutlookUntitledWindowDoctopPaneElement(), **{'Name':'File Tab'}), "Untitled message window didn't show up"
            
            #TC Step 6
            self.outlook.enterEmail__id(email_recepient_id)
            self.logger.info("Entered email id ...")
            
            self.outlook.enterEmail_subject(email_subject)
            self.logger.info("Entered email Subject ...")
            
            self.outlook.enterEmail_message_and_click_on_Send(email_content)
            self.logger.info("Entered email Content and clicked on Send.....")
            self.utils.sleepUntil(2)
            
            #TC Step 7
            self.utils.sleepUntil(15)
            
            assert self.outlook.isOutlookSentEmailElementExist(), "Didn't receive email in Inbox...something went wrong"
            self.logger.info("Email is received successfully")
            
            self.outlook.clickEmailReceivedInOutlookMainWindow()
            self.logger.info("Clicked on received Email")
            
            self.utils.sleepUntil(3)
            self.outlook.deleteEmailReceived()
            self.utils.sleepUntil(2)
            
            #TC Step 8
            self.outlook.clickExitMenuOption()
            self.logger.info("clicked on File->Exit of Outlook main window")
            
            #TC Step 9
            self.utils.sleepUntil(2)
            assert self.outlook.isOutlookProcessExist()==False, "Outlook process OUTLOOK.EXE still exist"
    
            return True, "", func.co_name
        
        except (AssertionError, AttributeError, TypeError, WebDriverException, NoSuchElementException) as e:
            return False, e, func.co_name
            
        finally:
            '''kill the web driver '''
            pid.terminate()
            self.logger.info("Killed the webDriver process of Outlook app")  
            
            
            
    '''TC ID : W10DA_20 ''' 
    def test_outlook_compose_mail_save_draft_testcase_id_W10DA_20(self):
        try:
            func = inspect.currentframe().f_back.f_code
            self.logger.info("Starting execution of function {} :".format(func.co_name))
            
            '''start webdriver '''
            pid = self.utils.startWebDriver()
            self.logger.info("Launched the WebDriver of Outlook app... ")
            
            
            #TC Step 1
            self.logger.info("Checking and killing the process OUTLOOK.exe if any before executing the test case")
            self.outlook.killProcess()
            
            #TC Step 2
            self.outlook.startOutlook_via_StartMenu()
            self.utils.sleepUntil(3)
            assert self.outlook.isOutlookProcessExist(), "Outlook process OUTLOOK.EXE didn't start properly"
            
            ''' Connect the already running Outlook process to winium Driver instead of opening new one.'''
            self.outlook.connectOutlookToWebDriver()
            assert self.outlook.isOutlookElementExist(self.outlook.getOutlookMainWindowDoctopPaneElement(),**{'Name':'New Email'}), "Outlook app didnt open properly"
            
            self.logger.info("New Email Element is preset")
            
            #TC Step 3
            self.outlook.clickNewMail()
            self.logger.info("Clicked on New Email element in outlook")
            self.utils.sleepUntil(2)
            
            assert self.outlook.isOutlookElementExist(self.outlook.getOutlookUntitledWindowDoctopPaneElement(), **{'Name':'File Tab'}), "Untitled message window didnt show up"
            
            #TC Step 4
            self.outlook.enterEmail__id(email_recepient_id)
            self.logger.info("Entered email id ...")
            
            self.outlook.enterEmail_subject(email_subject)
            self.logger.info("Entered email Subject ...")
            
            self.outlook.enterEmail_message(email_content)
            self.logger.info("Entered email Content .....")
            self.utils.sleepUntil(2)
            
            self.outlook.clickSaveIconOnUntitledMessageWindow()
            self.utils.sleepUntil(2)
            assert self.outlook.isOutlookElementExist(self.outlook.getOutlookUntitledWindowDoctopPaneElement(), **{'Name':'File Tab'}), "Untitled message window didnt show up"
            
            #TC Step 5
            self.outlook.clickCloseMenuOptionOfUntitledWindow()
            self.utils.sleepUntil(2)
            
            assert self.outlook.isOutlookElementExist(self.outlook.getOutlookMainWindowDoctopPaneElement(),**{'Name':'New Email'}), "Outlook app didnt show up properly"
            
            #TC Step 6
            self.outlook.clickOnEmailDraftFolder()
            self.utils.sleepUntil(2)
            
            assert self.outlook.isOutlookDraftedEmailElementExist(), "Didn't receive email in Draft...something went wrong"
            self.logger.info("Email is drafted successfully")
            
            self.outlook.clickEmailDraftedInOutlookMainWindow()
            self.logger.info("Clicked on drafted Email")
            self.utils.sleepUntil(2)
             
#             #TC Step 7
            self.outlook.clickExitMenuOption()
            self.logger.info("clicked on File->Exit of Outlook main window")
#             
#             #TC Step 8
            self.utils.sleepUntil(2)
            assert self.outlook.isOutlookProcessExist()==False, "Outlook process OUTLOOK.EXE still exist"
#     
            return True, "", func.co_name
        
        except (AssertionError, AttributeError, TypeError, WebDriverException, NoSuchElementException) as e:
            return False, e, func.co_name
            
        finally:
            '''kill the web driver '''
            pid.terminate()
            self.logger.info("Killed the webDriver process of Outlook app")  
         