'''
Created on Jul 23, 2018

@author: bkotamahanti
'''

import logging
import unittest
import pytest
from com.gilead.main_package.main import Main


class GUI_tests(unittest.TestCase):
    
    main = Main()
    logger = logging        
   
    def setUp(self):
        pass
    
    def tearDown(self):
        pass
    
    @pytest.mark.run(order=1)
    def test_notepad_save_and_open_file_testcase_id_W10DA_1(self):
        _testcase = None
        self.logger.info("===================> test_notepad_save_and_open_file <==========================================================")
        x,e, func_name = self.main.notepad_save_and_open_file_testcase_id_W10DA_1()
        if x is True:
            self.logger.info("Notepad testcase {} passed".format(func_name))
        else:    
            self.assertTrue(x, self.logger.debug("Notepad testcase {} failed due to reason: '".format(func_name)+str(e)+"'"))
        self.logger.info("=============================================================================")
    
    @pytest.mark.run(order=2)
    def test_notepad_dont_save_file_testcase_id_W10DA_2(self):
        self.logger.info("===================> test_notepad_dont_save_file <==========================================================")
        x, e, func_name = self.main.notepad_dont_save_file_testcase_id_W10DA_2()
        if x is True:
            self.logger.info("Notepad testcase {} passed".format(func_name))
        else:    
            self.assertTrue(x, self.logger.debug("Notepad testcase {} failed due to reason: '".format(func_name)+str(e)+"'"))
        
        self.logger.info("=============================================================================")
     
    @pytest.mark.run(order=3)
    def test_notepad_save_delete_open_file_testcase_id_W10DA_3(self):
        self.logger.info("===================> test_notepad_save_delete_open_file <==========================================================")
        x,e, func_name = self.main.notepad_save_delete_open_file_testcase_id_W10DA_3()
        if x is True:
            self.logger.info("Notepad testcase {} passed".format(func_name))
        else:    
            self.assertTrue(x, self.logger.debug("Notepad testcase {} failed due to reason: '".format(func_name)+str(e)+"'"))
        self.logger.info("=============================================================================")
     
    
    @pytest.mark.run(order=4)
    def test_calculator_open_perform_arithemetic_operations_close_app_testcase_id_W10DA_4(self):
        self.logger.info("=====================================> test_calculator_open_close_app <=============================")
        x,e, func_name = self.main.calculator_open_perform_arithemetic_operations_close_app_testcase_id_W10DA_4()
        if x is True:
            self.logger.info("Calculator testcase {} passed".format(func_name))
        else:    
            self.assertTrue(x, self.logger.debug("Calculator testcase {} failed due to reason: '".format(func_name) +str(e)+"'"))
        self.logger.info("=============================================================================")
    
    
    @pytest.mark.run(order=5)
    def test_paint_open_do_paint_close_save_app_open_testcase_id_W10DA_5(self):
        self.logger.info("===================> test_paint_open_do_paint_close_save_app_open_testcase_id_W10DA_5 <==========================================================")
        x,e, func_name = self.main.paint_open_do_paint_close_save_app_open_testcase_id_W10DA_5()
        if x is True:
            self.logger.info("Paint testcase {} passed".format(func_name))
        else:    
            self.assertTrue(x, self.logger.debug("Paint testcase  {} failed due to reason: '".format(func_name)+str(e)+"'"))
        self.logger.info("=============================================================================")
    
    @pytest.mark.run(order=6)
    def test_paint_open_do_paint_close_dont_save_testcase_id_W10DA_6(self):
        self.logger.info("===================> test_paint_open_do_paint_close_dont_save_testcase_id_W10DA_6 <==========================================================")
        x,e, func_name = self.main.paint_open_do_paint_close_dont_save_testcase_id_W10DA_6()
        if x is True:
            self.logger.info("Paint testcase {} passed".format(func_name))
        else:    
            self.assertTrue(x, self.logger.debug("Paint testcase {} failed due to reason: '".format(func_name)+str(e)+"'"))
        self.logger.info("=============================================================================")
    
    @pytest.mark.run(order=7)
    def test_paint_save_delete_open_file_testcase_id_W10DA_7(self):
        self.logger.info("===================> test_paint_save_delete_open_file_testcase_id_W10DA_7 <==========================================================")
        x,e, func_name = self.main.paint_save_delete_open_file_testcase_id_W10DA_7()
        if x is True:
            self.logger.info("Paint testcase {} passed".format(func_name))
        else:    
            self.assertTrue(x, self.logger.debug("Paint testcase {} failed due to reason: '".format(func_name)+str(e)+"'"))
        self.logger.info("=============================================================================")
        
        
    @pytest.mark.run(order=8)    
    def test_symantec_endpoint_protection_open_close_testcase_id_W10DA_8(self):
        self.logger.info("===================> test_symantec_endpoint_protection_open_close_app <==========================================================")
        x, e, func_name = self.main.symantec_endpoint_protection_open_close_testcase_id_W10DA_8()
        
        if x is True:
            self.logger.info("Symantec End point protection testcase {} passed".format(func_name))
        else:    
            self.assertTrue(x, self.logger.debug("Symantec End point protection testcase {} failed due to reason: '".format(func_name)+str(e)+"'"))
        
        self.logger.info("=============================================================================")  
    
    
    @pytest.mark.run(order=26)    
    def test_symantec_endpoint_protection_run_active_scan_testcase_id_W10DA_26(self):
        self.logger.info("===================> test_symantec_endpoint_protection_run_active_scan_testcase_id_W10DA_26 <==========================================================")
        x, e, func_name = self.main.symantec_endpoint_protection_run_active_scan_testcase_id_W10DA_26()
        
        if x is True:
            self.logger.info("Symantec End point protection Run active scan testcase {} passed".format(func_name))
        else:    
            self.assertTrue(x, self.logger.debug("Symantec End point protection Run active scan testcase {} failed due to reason: '".format(func_name)+str(e)+"'"))
        
        self.logger.info("=============================================================================")      
    
    @pytest.mark.run(order=19)
    def test_outlook_compose_mail_check_mail_inbox_close_testcase_id_W10DA_19(self):
        self.logger.info("===================> test_outlook_compose_mail_check_mail_inbox_close_testcase_id_W10DA_19 <==========================================================")
        x,e, func_name = self.main.outlook_compose_mail_check_mail_inbox_close_testcase_id_W10DA_19()
        if x is True:
            self.logger.info("Outlook New mail testcase {} passed".format(func_name))
        else:    
            self.assertTrue(x, self.logger.debug("Outlook New mail testcase {} failed due to reason: '".format(func_name)+str(e)+"'"))
        self.logger.info("=============================================================================")    
        
    @pytest.mark.run(order=20)
    def test_outlook_compose_mail_save_draft_testcase_id_W10DA_20(self):
        self.logger.info("===================> test_outlook_compose_mail_save_draft_testcase_id_W10DA_20 <==========================================================")
        x,e, func_name = self.main.test_outlook_compose_mail_save_draft_testcase_id_W10DA_20()
        if x is True:
            self.logger.info("Outlook New mail draft testcase {} passed".format(func_name))
        else:    
            self.assertTrue(x, self.logger.debug("Outlook New mail Draft testcase {} failed due to reason: '".format(func_name)+str(e)+"'"))
        self.logger.info("=============================================================================")            

# Execute Tests
def main():
    pytest.main(['GUI_tests.py', '-s'])


if __name__ == '__main__':
    main()